#ifndef WIMPDQEVENT_H
#define WIMPDQEVENT_H

#include "EventBase.h"

#include <vector>
#include <string>

#include "rqlibProjectHeaders.h"

#include "DetectorRQs.h"

class WIMPDQEvent : public EventBase {

 public:

  WIMPDQEvent(EventBase* eventBase);
  virtual ~WIMPDQEvent();

  void deriveEvtRQs();
  
  TTreeReaderValue<RQ::EventHeader>   header;
  WIMPDQ::DetectorRQs tpc;
  TTreeReaderValue<RQ::SingleScatter> ss;

  unsigned long prevTriggerTime_s;
  unsigned long prevTriggerTime_ns;
  long          sumLiveTime_ns;
  float         ss_r2;
  float         ss_r;
  float         ss_R2;
  float         ss_R;
  
 private:

};


#endif // WIMPDQEVENT_H
