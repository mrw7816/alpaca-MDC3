#ifndef MDC3_WIMPDQ_H
#define MDC3_WIMPDQ_H

#include "Analysis.h"

#include "EventBase.h"
#include "CutsMDC3_WIMPDQ.h"
#include "WIMPDQEvent.h"

#include <TTreeReader.h>
#include <string>

class MDC3_WIMPDQ: public Analysis {

public:
  MDC3_WIMPDQ(); 
  ~MDC3_WIMPDQ();

  void Initialize();
  void Execute();
  void Finalize();

protected:
  CutsMDC3_WIMPDQ* m_cutsWIMPDQ;
  ConfigSvc* m_conf;

  WIMPDQEvent* m_evt;
};

#endif
