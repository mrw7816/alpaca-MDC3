#ifndef CutsMDC3_WIMPDQ_H
#define CutsMDC3_WIMPDQ_H

#include "WIMPDQEvent.h"

#include "TString.h"

class CutsMDC3_WIMPDQ  {

public:
  CutsMDC3_WIMPDQ(WIMPDQEvent* event);
  ~CutsMDC3_WIMPDQ();
  bool MDC3_WIMPDQCutsOK();
  
private:
  
  WIMPDQEvent* m_evt;
  
};

#endif
