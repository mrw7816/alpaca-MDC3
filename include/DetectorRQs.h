#ifndef WIMPDQ_DETECTORRQS_H
#define WIMPDQ_DETECTORRQS_H

#include <TTreeReader.h>
#include <TString.h>
#include <TVector3.h>

#include <string>
#include <vector>
#include <map>

namespace WIMPDQ {
  
  class DetectorRQs {

  public:
    DetectorRQs(TTreeReader&, std::string);
    virtual ~DetectorRQs();

    TString baseName;
    TTreeReaderValue<int>                 nPulses;
    TTreeReaderValue<std::vector<int>>    pID;
    TTreeReaderValue<std::vector<float>>  pArea;
    TTreeReaderValue<std::vector<int>>    pStart;
    TTreeReaderValue<std::vector<int>>    pEnd;
    TTreeReaderValue<std::vector<float>>  s1prob;
    TTreeReaderValue<std::vector<float>>  s2prob;
    TTreeReaderValue<std::vector<float>>  seProb;
    TTreeReaderValue<std::vector<float>>  speProb;
    TTreeReaderValue<std::vector<float>>  mpeProb;
    TTreeReaderValue<std::vector<float>>  otherProb;
    TTreeReaderValue<std::vector<float>>  mercX, mercY;
    TTreeReaderValue<std::vector<float>>  tba;
    TTreeReaderValue<std::vector<int>>    bPC;
    TTreeReaderValue<std::vector<int>>    tPC;
    TTreeReaderValue<std::vector<int>>    pc;
    TTreeReaderValue<std::vector<int>>    HGLGpID;
    TTreeReaderValue<std::vector<int>>    aft90;
    TTreeReaderValue<std::vector<int>>    aft10;

    TTreeReaderValue<std::vector<std::vector<int>>>   chID;
    TTreeReaderValue<std::vector<std::vector<float>>> chArea;

    // derived RQs
    std::vector<int>*          pDuration;
    std::vector<std::string>*  pClass;

    void deriveDetRQs();
    int   maxChID(const int pID);
    float maxFrac(const int pID);

  private:

  };
}

#endif // WIMPDQ_DETECTORRQS_H
