Applies data quality cuts in a specific order.
After each cut:
1. Produces same set of histograms of events that pass all cuts up to and including the current one.
2. Produces same set of histograms of events that pass all cuts up to but NOT including the current one.

Order of current cuts:
SS (single scatter),
FIDZ (fiducial Z),
FIDR (fiducial R),
SKINdome,
SKINbarrel,
OD,
HEROI (high energy NR region of interest),
ROI (WIMP region of interest)

Run instructions found in ALPACA documentation: 
https://docs.lz-git-pages.ua.edu/softwaredocs/analysis/analysiswithrqs/intro.html