#!/bin/bash
echo "This is to create a submodule so you can run an analysis on ALPACA output"
echo "You need to run this from inside your module!"
if [ "$#" -ne 2 ]; then
    echo "You must enter a command line arguments for the Analysis name and the number it is refered to in the config file"
else
    NAME=$1
    NUMBER=$2
    FILE=$(ls *Main.cxx)
    echo "Creating sub analysis: $NAME ran by config variable/command, whichAna $NUMBER"
    mkdir temp
    cd temp
    git clone git@lz-git.ua.edu:analysis/alpaca/SkeletonModule.git .
    sh RenameSkeleton.sh $NAME

    mv src/*$NAME* ../src/.
    mv include/*$NAME* ../include/.

    cd ..
    rm -rf temp

    sed -i "s|#include <string>|#include \"$NAME.h\"\n#include <string>|g" $FILE

    cutoffLine="// Create analysis code and run"
    while read LINE; do
        echo $LINE >> testing.txt
    if [[ "$LINE" = $cutoffLine ]];then
        echo "if (config->configFileVarMap[\"whichAna\"]==${NUMBER}){" >>testing.txt
        echo "ana = new ${NAME}();"   >> testing.txt
        echo "ana->Run(config->FileList, config->OutName);">>testing.txt
        echo "}" >> testing.txt
    fi
    done < $FILE
    mv testing.txt $FILE
    echo "New sub-module has been created - checkout the src and include files for where this has been created"
fi