#include "DetectorRQs.h"
#include <iostream>

using WIMPDQ::DetectorRQs;

DetectorRQs::DetectorRQs(TTreeReader& reader, std::string detector):
  baseName    (detector),
  nPulses     (reader, baseName+".nPulses"),
  pID         (reader, baseName+".pulseID"),
  pArea       (reader, baseName+".pulseArea_phd"),
  pStart      (reader, baseName+".pulseStartTime_ns"),
  pEnd        (reader, baseName+".pulseEndTime_ns"),
  s1prob      (reader, baseName+".s1Probability"),
  s2prob      (reader, baseName+".s2Probability"),
  seProb      (reader, baseName+".singleElectronProbability"),
  speProb     (reader, baseName+".singlePEprobability"),
  mpeProb     (reader, baseName+".multiplePEprobability"),
  otherProb   (reader, baseName+".otherProbability"),
  mercX       (reader, baseName+".s2Xposition_cm"),
  mercY       (reader, baseName+".s2Yposition_cm"),
  tba         (reader, baseName+".topBottomAsymmetry"),
  bPC         (reader, baseName+".bottomPhotonCount"),
  tPC         (reader, baseName+".topPhotonCount"),
  pc          (reader, baseName+".photonCount"),
  HGLGpID     (reader, baseName+".HGLGpulseID"),
  chID        (reader, baseName+".chID"),
  chArea      (reader, baseName+".chPulseArea_phd"),
  aft90       (reader, baseName+".areaFractionTime90_ns"),
  aft10       (reader, baseName+".areaFractionTime10_ns"),
  pDuration   (new std::vector<int>()),
  pClass      (new std::vector<std::string>())
{}

DetectorRQs::~DetectorRQs()
{}


void DetectorRQs::deriveDetRQs() {

  pDuration->clear();
  pClass->clear();
  for (int p=0; p<*nPulses; ++p) {
    pDuration->push_back((*pEnd)[p] - (*pStart)[p]);
    if      ( (*s1prob)[p] > 0.9 ) {
      pClass->push_back("S1");
    }
    else if ( (*s2prob)[p] > 0.9 ) {
      pClass->push_back("S2");
    }
    else if ( (*seProb)[p] > 0.9 ) {
      pClass->push_back("SE");
    }
    else if ( (*speProb)[p] > 0.9 ) {
      pClass->push_back("SPE");
    }
    else if ( (*mpeProb)[p] > 0.9 ) {
      pClass->push_back("MPE");
    }
    else if ( (*otherProb)[p] > 0.9 ) {
      pClass->push_back("OTHER");
    }
    else {
      pClass->push_back("NONE");
    }
  }
  
}

int DetectorRQs::maxChID(const int pulseID) {
  int maxCh = -1;
  float maxChArea = 0;
  for (size_t iCh = 0; iCh < (*chID)[pulseID].size(); ++iCh) {
    const int channelID = (*chID)[pulseID][iCh];
    if ( (*chArea)[pulseID][iCh] > maxChArea ) {
      maxCh = channelID;
      maxChArea = (*chArea)[pulseID][iCh];
    }
  }
  return maxCh;
}

float DetectorRQs::maxFrac(const int pulseID) {
  float maxFrac = 0;
  int maxCh = -1;
  float maxChArea = 0;
  for (size_t iCh = 0; iCh < (*chID)[pulseID].size(); ++iCh) {
    const int channelID = (*chID)[pulseID][iCh];
    if ( (*chArea)[pulseID][iCh] > maxChArea ) {
      maxCh = channelID;
      maxChArea = (*chArea)[pulseID][iCh];
    }
  }
  maxFrac = maxChArea / (*pArea)[pulseID];
  return maxFrac;
}


