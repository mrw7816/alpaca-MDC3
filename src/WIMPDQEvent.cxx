#include "WIMPDQEvent.h"
#include "TMath.h"

WIMPDQEvent::WIMPDQEvent(EventBase* base):
  header (base->m_reader, "eventHeader."),
  tpc    (base->m_reader, "pulsesTPC"),
  ss     (base->m_reader, "ss.")
{
  prevTriggerTime_s = 0;
  prevTriggerTime_ns = 0;
  sumLiveTime_ns = 0;
}

WIMPDQEvent::~WIMPDQEvent()
{}


void WIMPDQEvent::deriveEvtRQs() {
  tpc.deriveDetRQs();

  if (prevTriggerTime_s != 0 && prevTriggerTime_ns != 0) {  // do nothing for first event in chain
    long deltaT = ( (header->triggerTimeStamp_s - prevTriggerTime_s)*1E9 +
                    ((long)header->triggerTimeStamp_ns - (long)prevTriggerTime_ns) );
    sumLiveTime_ns += deltaT;
  }
  prevTriggerTime_s = header->triggerTimeStamp_s;
  prevTriggerTime_ns = header->triggerTimeStamp_ns;

  if (ss->nSingleScatters == 1) {
    ss_r2 = ss->x_cm*ss->x_cm + ss->y_cm*ss->y_cm;
    ss_r  = TMath::Sqrt(ss_r2);
    ss_R2 = ss->correctedX_cm*ss->correctedX_cm + ss->correctedY_cm*ss->correctedY_cm;
    ss_R  = TMath::Sqrt(ss_R2);
  }
  else {
    ss_r2 = -225;
    ss_r = -15;
    ss_R2 = -225;
    ss_R = -15;
  }
  
}
