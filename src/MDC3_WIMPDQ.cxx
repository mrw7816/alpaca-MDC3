#include "EventBase.h"
#include "Analysis.h"
#include "HistSvc.h"
#include "Logger.h"

#include "CutsBase.h"
#include "CutsMDC3_WIMPDQ.h"
#include "ConfigSvc.h"
#include "MDC3_WIMPDQ.h"

#include "TMath.h"
#include "TH2.h"
#include "TH2F.h"

#include <iomanip>


using namespace std;

// Detector constants
static const float kRmax =  68.8;    //[cm]
static const float kZmin =   2.0;    //[cm]
static const float kZmax = 132.6;    //[cm]
static const float ROIS1 = 80;       //[phd]
static const float HEROIS1 = 1000;       //[phd]
static const float skinDomeMaxPhd = 5; //[phd]
static const float skinBarrelMaxPhd = 5; //[phd]
static const float ODMaxPhd = 10;   //[pdh]

namespace {
  //________________________________________________________
  void Loggify(TAxis* axis) {
    int bins = axis->GetNbins();

    Axis_t from = TMath::Log10(axis->GetXmin());
    Axis_t to = TMath::Log10(axis->GetXmax());
    Axis_t width = (to - from) / bins;
    Axis_t *new_bins = new Axis_t[bins + 1];
  
    for (int i = 0; i <= bins; i++) new_bins[i] = TMath::Power(10, from + i * width);
    axis->Set(bins, new_bins); 
    delete[] new_bins; 
  }
  
  TH1* LoggifyX(TH1* h)  { Loggify(h->GetXaxis()); return h; }
  TH2* LoggifyXY(TH2* h) { Loggify(h->GetXaxis()); Loggify(h->GetYaxis()); return h;}
  TH2* LoggifyX(TH2* h)  { Loggify(h->GetXaxis()); return h; }
  TH2* LoggifyY(TH2* h)  { Loggify(h->GetYaxis()); return h; }

  // List of all the cuts you are going to apply
  enum CX {SS,FIDZ,FIDR,SKINdome,SKINbarrel,OD,HEROI,ROI};

  // Order matters! order must match listing in CX enum above
  static const vector<CX> allCXs = {
    CX::SS,
    CX::FIDZ,
    CX::FIDR,
    CX::SKINdome,
    CX::SKINbarrel,
    CX::OD,
    CX::HEROI,
    CX::ROI
  };

  // Define all cuts
  bool isSS(WIMPDQEvent* m_evt) {
    return m_evt->ss->nSingleScatters >= 1;
  }

  bool isFIDZ(WIMPDQEvent* m_evt) {
    return m_evt->ss->correctedZ_cm >= kZmin && m_evt->ss->correctedZ_cm >= kZmax;
  }

  bool isFIDR(WIMPDQEvent* m_evt) {
    return m_evt->ss_R <= kRmax;
  }

  bool isSKINdome(WIMPDQEvent* m_evt) {
    return m_evt->ss->skinDomeArea <= skinDomeMaxPhd;
  }

  bool isSKINbarrel(WIMPDQEvent* m_evt) {
    return m_evt->ss->skinBarrelArea <= skinBarrelMaxPhd;
  }

  bool isOD(WIMPDQEvent* m_evt) {
    return m_evt->ss->odPromptArea <= ODMaxPhd;
  }

  bool isHEROI(WIMPDQEvent* m_evt) {
    return m_evt->ss->correctedS1Area_phd <= HEROIS1;
  }
  
  bool isROI(WIMPDQEvent* m_evt) {
    return m_evt->ss->correctedS1Area_phd <= ROIS1;
  }

  const TString CXtoStr(CX cx)
  {
    if      (cx == CX::SS)    return "SS";
    else if (cx == CX::FIDZ)  return "FidZ";
    else if (cx == CX::FIDR)  return "FidR";
    else if (cx == CX::SKINdome)  return "SkinDome";
    else if (cx == CX::SKINbarrel)  return "SkinBarrel";
    else if (cx == CX::OD)    return "OD";
    else if (cx == CX::HEROI)   return "HEROI";
    else if (cx == CX::ROI)   return "ROI";
    else return "NONE";
  }

  bool EvalCX(WIMPDQEvent* m_evt, const CX cx) {
    //cout << "Got to eval" << endl;
    if (cx == CX::SS)   return isSS(m_evt);
    if (cx == CX::FIDZ) return isFIDZ(m_evt);
    if (cx == CX::FIDR) return isFIDR(m_evt);
    if (cx == CX::SKINdome) return isSKINdome(m_evt);
    if (cx == CX::SKINbarrel) return isSKINbarrel(m_evt);
    if (cx == CX::OD)   return isOD(m_evt);
    if (cx == CX::HEROI)  return isHEROI(m_evt);
    if (cx == CX::ROI)  return isROI(m_evt);
    return true;
  }
  
  bool AccumCX(WIMPDQEvent* m_evt, const CX cx, const bool accept = 1, const vector<CX> ignore = {}) {
    // Accumulate cuts, up to cx, ignoring cuts as told. If want inversion of this cut,
    // we apply all cuts prior to cx, and then apply !EvalCX(cx)
    bool result = 1;
    //cout << "CX  is " << CXtoStr(cx) << endl;
    for (const CX icx : allCXs) {
      //cout << "iCX  is " << CXtoStr(icx) << endl;
      const bool ignoreCX = std::find(ignore.begin(), ignore.end(), icx) != ignore.end();
      if (icx < cx) {
	if (ignoreCX)
	  //if told to ignore this cut, ignore it
	  result *= 1;
	else
	  result *= EvalCX(m_evt, icx);
      }
      else if (icx == cx) {
	//cout << "CXs are the same" << endl;
	if (accept && !ignoreCX)
	  result *= EvalCX(m_evt, icx);
	else if (!accept)
	  result *= !EvalCX(m_evt, icx);
      }
    }
    //cout << "Returning AccumCX result: " << result << endl;
    return result;
  }
}

// Constructor
MDC3_WIMPDQ::MDC3_WIMPDQ()
  : Analysis(),
    m_evt(new WIMPDQEvent(m_event))
{
  // List the branches required below, see full list here *:::: LINK TO SOFTWARE DOCS  ::::*
  // NOTE : if you try and use a variable in a branch that has NOT been loaded you will get a seg fault
  // there is not a error message for this currently. If you get a seg fault first thing to check 
  // is that you are including the required branch that variable lives on. 
  //
  //m_event->IncludeBranch("pulsesTPC");
  //m_event->IncludeBranch("ss");
  //
  ////////

  m_event->Initialize();

  // Setup logging
  logging::set_program_name("MDC3_WIMPDQ Analysis");
  // Logging level: error = 1, warning = 2, info = 3, debug = 4, verbose = 5
  
  m_cutsWIMPDQ = new CutsMDC3_WIMPDQ(m_evt);
  
  //create a config instance, this can be used to call the config variables. 
  m_conf = ConfigSvc::Instance();
}

// Destructor
MDC3_WIMPDQ::~MDC3_WIMPDQ() {
  delete m_cutsWIMPDQ;
  delete m_evt;
}

// Called before event loop
void MDC3_WIMPDQ::Initialize() {
  INFO("Initializing MDC3_WIMPDQ Analysis");

  //Set up all the bins you want
  const int   nS1bins = 500;
  const float s1min = 1.e0;
  const float s1max = 1.e6;
  const int   nS2bins = 500;
  const float s2min = 1.e1;
  const float s2max = 1.e8;
  const int   nS2S1bins = 500;
  const float s2s1min = 1.e1;
  const float s2s1max = 1e5;
  const int   nR2bins = 500;
  const float r2min = -1.e3;
  const float r2max = 8.e3;
  const int   nDriftBins = 500;
  const float driftMin = -1.e2;
  const float driftMax = 1.e3;
  const int   nZbins = 500;
  const float zMin = -10;
  const float zMax = 150;
  const int   nTBAbins = 500;
  const float tbaMin = -1.;
  const float tbaMax = 1.;
  const int   nS2widthBins = 500;
  const float s2widthMin = 0.;
  const float s2widthMax = 20.;
  const int   nXYbins = 500;
  const float XYmin = -1.e2;
  const float XYmax = 1.e2;

  // ALPACA owns the booked histogram. We use temp pointers to manipulate the histograms.
  TH1* htmp = NULL;

  // for each cut
  for (const CX cx: allCXs) {

    // create all the histograms in the right folders, with the right names
    htmp = m_hists->BookHist( Form("%s/h_%s_S2_S1",      CXtoStr(cx).Data(), CXtoStr(cx).Data()), nS1bins, s1min, s1max, nS2bins, s2min, s2max);  htmp->SetTitle("; S1 [phd]; S2 [phd]");          LoggifyXY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("%s/h_%s_cS2_cS1wimp",CXtoStr(cx).Data(), CXtoStr(cx).Data()), 250, 0, 80, nS2bins, s2min, 1.e5);  htmp->SetTitle("; S1 [phd]; S2 [phd]");          LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("%s/h_%s_cS2_cS1henr",CXtoStr(cx).Data(), CXtoStr(cx).Data()), 250, 0, 1000, nS2bins, s2min, 1.e7);  htmp->SetTitle("; S1 [phd]; S2 [phd]");          LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("%s/h_%s_cS2_cS1",    CXtoStr(cx).Data(), CXtoStr(cx).Data()), nS1bins, s1min, s1max, nS2bins, s2min, s2max);  htmp->SetTitle("; S1_{c} [phd]; S2_{c} [phd]");  LoggifyXY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("%s/h_%s_cS2log_cS1", CXtoStr(cx).Data(), CXtoStr(cx).Data()), nS1bins, s1min, s1max, nS2bins, TMath::Log10(s2min), TMath::Log10(s2max));  htmp->SetTitle("; S1_{c} [phd]; log (S2_{c} [phd])");  LoggifyX((TH2*)htmp);
    htmp = m_hists->BookHist( Form("%s/h_%s_cS2cS1_cS1", CXtoStr(cx).Data(), CXtoStr(cx).Data()), nS1bins, s1min, s1max, nS2S1bins, s2s1min, s2s1max);  htmp->SetTitle("; S1_{c} [phd]; S2_{c} / S1_{c}");  LoggifyXY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("%s/h_%s_r2_drift",   CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nDriftBins, -driftMax, -driftMin);  htmp->SetTitle("; R^{2} [cm^{2}]; -drift [us]");
    htmp = m_hists->BookHist( Form("%s/h_%s_cR2_cZ",     CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nZbins, zMin, zMax);  htmp->SetTitle("; R_{c}^{2} [cm^{2}]; Z_{c} [cm]");
    htmp = m_hists->BookHist( Form("%s/h_%s_x_y",        CXtoStr(cx).Data(), CXtoStr(cx).Data()), nXYbins, XYmin, XYmax, nXYbins, XYmin, XYmax);  htmp->SetTitle("; X [cm]; Y [cm]");
    htmp = m_hists->BookHist( Form("%s/h_%s_cX_cY",      CXtoStr(cx).Data(), CXtoStr(cx).Data()), nXYbins, XYmin, XYmax, nXYbins, XYmin, XYmax);  htmp->SetTitle("; X_{c} [cm]; Y_{c} [cm]");
    htmp = m_hists->BookHist( Form("%s/h_%s_cS1_cR2",    CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nS1bins, s1min, s1max);  htmp->SetTitle("; R_{c}^{2} [cm^{2}]; S1_{c} [phd]");  LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("%s/h_%s_cS2_cR2",    CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nS2bins, s2min, s2max);  htmp->SetTitle("; R_{c}^{2} [cm^{2}]; S2_{c} [phd]");  LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("%s/h_%s_cS2cS1_cR2", CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nS2S1bins, s2s1min, s2s1max);  htmp->SetTitle("; R_{c}^{2} [cm^{2}]; S2_{c}/S1_{c}");  LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("%s/h_%s_cS1_cZ",     CXtoStr(cx).Data(), CXtoStr(cx).Data()), nZbins, zMin, zMax, nS1bins, s1min, s1max);     htmp->SetTitle("; Z_{c} [cm]; S1_{c} [phd]");  LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("%s/h_%s_cS2_cZ",     CXtoStr(cx).Data(), CXtoStr(cx).Data()), nZbins, zMin, zMax, nS2bins, s2min, s2max);     htmp->SetTitle("; Z_{c} [cm]; S2_{c} [phd]");  LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("%s/h_%s_cS2cS1_cZ",  CXtoStr(cx).Data(), CXtoStr(cx).Data()), nZbins, zMin, zMax, nS2S1bins, s2s1min, s2s1max);  htmp->SetTitle("; Z_{c} [cm]; S2_{c}/S1_{c}");  LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("%s/h_%s_s1tba_cZ",   CXtoStr(cx).Data(), CXtoStr(cx).Data()), nZbins, zMin, zMax, nTBAbins, tbaMin, tbaMax);  htmp->SetTitle("; Z_{c} [cm]; S1 TBA");
    htmp = m_hists->BookHist( Form("%s/h_%s_s2tba_cZ",   CXtoStr(cx).Data(), CXtoStr(cx).Data()), nZbins, zMin, zMax, nTBAbins, tbaMin, tbaMax);  htmp->SetTitle("; Z_{c} [cm]; S2 TBA");
    htmp = m_hists->BookHist( Form("%s/h_%s_s1tba_drift",CXtoStr(cx).Data(), CXtoStr(cx).Data()), nDriftBins, driftMin, driftMax, nTBAbins, tbaMin, tbaMax);  htmp->SetTitle("; drift [us]; S1 TBA");
    htmp = m_hists->BookHist( Form("%s/h_%s_s2tba_drift",CXtoStr(cx).Data(), CXtoStr(cx).Data()), nDriftBins, driftMin, driftMax, nTBAbins, tbaMin, tbaMax);  htmp->SetTitle("; drift [us]; S2 TBA");
    htmp = m_hists->BookHist( Form("%s/h_%s_s1tba_cR2",  CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nTBAbins, tbaMin, tbaMax);  htmp->SetTitle("; R_{c}^{2} [cm^{2}]; S1 TBA");
    htmp = m_hists->BookHist( Form("%s/h_%s_s2tba_cR2",  CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nTBAbins, tbaMin, tbaMax);  htmp->SetTitle("; R_{c}^{2} [cm^{2}]; S2 TBA");
    htmp = m_hists->BookHist( Form("%s/h_%s_s2width_drift",CXtoStr(cx).Data(),CXtoStr(cx).Data()),nDriftBins, driftMin, driftMax, nS2widthBins, s2widthMin, s2widthMax);  htmp->SetTitle("; drift [us]; S2 width [us]");
    htmp = m_hists->BookHist( Form("%s/h_%s_s2width_cZ", CXtoStr(cx).Data(), CXtoStr(cx).Data()), nZbins, zMin, zMax, nS2widthBins, s2widthMin, s2widthMax);  htmp->SetTitle("; Z_{c} [cm]; S2 width [us]");
    htmp = m_hists->BookHist( Form("%s/h_%s_s2width_cR2",CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nS2widthBins, s2widthMin, s2widthMax);  htmp->SetTitle("; R_{c}^{2} [cm^{2}]; S2 width [us]");

    // Also create the anti histograms
    htmp = m_hists->BookHist( Form("n%s/h_n%s_S2_S1",      CXtoStr(cx).Data(), CXtoStr(cx).Data()), nS1bins, s1min, s1max, nS2bins, s2min, s2max);  htmp->SetTitle("; S1 [phd]; S2 [phd]");          LoggifyXY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cS2_cS1wimp",CXtoStr(cx).Data(), CXtoStr(cx).Data()), 250, 0, 80, nS2bins, s2min, 1.e5);  htmp->SetTitle("; S1 [phd]; S2 [phd]");          LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cS2_cS1henr",CXtoStr(cx).Data(), CXtoStr(cx).Data()), 250, 0, 1000, nS2bins, s2min, 1.e7);  htmp->SetTitle("; S1 [phd]; S2 [phd]");          LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cS2_cS1",    CXtoStr(cx).Data(), CXtoStr(cx).Data()), nS1bins, s1min, s1max, nS2bins, s2min, s2max);  htmp->SetTitle("; S1_{c} [phd]; S2_{c} [phd]");  LoggifyXY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cS2log_cS1", CXtoStr(cx).Data(), CXtoStr(cx).Data()), nS1bins, s1min, s1max, nS2bins, TMath::Log10(s2min), TMath::Log10(s2max));  htmp->SetTitle("; S1_{c} [phd]; log (S2_{c} [phd])");  LoggifyX((TH2*)htmp);
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cS2cS1_cS1", CXtoStr(cx).Data(), CXtoStr(cx).Data()), nS1bins, s1min, s1max, nS2S1bins, s2s1min, s2s1max);  htmp->SetTitle("; S1_{c} [phd]; S2_{c} / S1_{c}");  LoggifyXY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("n%s/h_n%s_r2_drift",   CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nDriftBins, -driftMax, -driftMin);  htmp->SetTitle("; R^{2} [cm^{2}]; -drift [us]");
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cR2_cZ",     CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nZbins, zMin, zMax);  htmp->SetTitle("; R_{c}^{2} [cm^{2}]; Z_{c} [cm]");
    htmp = m_hists->BookHist( Form("n%s/h_n%s_x_y",        CXtoStr(cx).Data(), CXtoStr(cx).Data()), nXYbins, XYmin, XYmax, nXYbins, XYmin, XYmax);  htmp->SetTitle("; X [cm]; Y [cm]");
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cX_cY",      CXtoStr(cx).Data(), CXtoStr(cx).Data()), nXYbins, XYmin, XYmax, nXYbins, XYmin, XYmax);  htmp->SetTitle("; X_{c} [cm]; Y_{c} [cm]");
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cS1_cR2",    CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nS1bins, s1min, s1max);  htmp->SetTitle("; R_{c}^{2} [cm^{c}]; S1_{c} [phd]");  LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cS2_cR2",    CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nS2bins, s2min, s2max);  htmp->SetTitle("; R_{c}^{2} [cm^{c}]; S2_{c} [phd]");  LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cS2cS1_cR2", CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nS2S1bins, s2s1min, s2s1max);  htmp->SetTitle("; R_{c}^{2} [cm^{c}]; S2_{c}/S1_{c}");  LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cS1_cZ",     CXtoStr(cx).Data(), CXtoStr(cx).Data()), nZbins, zMin, zMax, nS1bins, s1min, s1max);     htmp->SetTitle("; Z_{c} [cm]; S1_{c} [phd]");  LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cS2_cZ",     CXtoStr(cx).Data(), CXtoStr(cx).Data()), nZbins, zMin, zMax, nS2bins, s2min, s2max);     htmp->SetTitle("; Z_{c} [cm]; S2_{c} [phd]");  LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("n%s/h_n%s_cS2cS1_cZ",  CXtoStr(cx).Data(), CXtoStr(cx).Data()), nZbins, zMin, zMax, nS2S1bins, s2s1min, s2s1max);  htmp->SetTitle("; Z_{c} [cm]; S2_{c}/S1_{c}");  LoggifyY((TH2*)htmp);
    htmp = m_hists->BookHist( Form("n%s/h_n%s_s1tba_cZ",   CXtoStr(cx).Data(), CXtoStr(cx).Data()), nZbins, zMin, zMax, nTBAbins, tbaMin, tbaMax);  htmp->SetTitle("; Z_{c} [cm]; S1 TBA");
    htmp = m_hists->BookHist( Form("n%s/h_n%s_s2tba_cZ",   CXtoStr(cx).Data(), CXtoStr(cx).Data()), nZbins, zMin, zMax, nTBAbins, tbaMin, tbaMax);  htmp->SetTitle("; Z_{c} [cm]; S2 TBA");
    htmp = m_hists->BookHist( Form("n%s/h_n%s_s1tba_drift",CXtoStr(cx).Data(), CXtoStr(cx).Data()), nDriftBins, driftMin, driftMax, nTBAbins, tbaMin, tbaMax);  htmp->SetTitle("; drift [us]; S1 TBA");
    htmp = m_hists->BookHist( Form("n%s/h_n%s_s2tba_drift",CXtoStr(cx).Data(), CXtoStr(cx).Data()), nDriftBins, driftMin, driftMax, nTBAbins, tbaMin, tbaMax);  htmp->SetTitle("; drift [us]; S2 TBA");
    htmp = m_hists->BookHist( Form("n%s/h_n%s_s1tba_cR2",  CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nTBAbins, tbaMin, tbaMax);  htmp->SetTitle("; R_{c}^{2} [cm^{2}]; S1 TBA");
    htmp = m_hists->BookHist( Form("n%s/h_n%s_s2tba_cR2",  CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nTBAbins, tbaMin, tbaMax);  htmp->SetTitle("; R_{c}^{2} [cm^{2}]; S2 TBA");
    htmp = m_hists->BookHist( Form("n%s/h_n%s_s2width_drift",CXtoStr(cx).Data(),CXtoStr(cx).Data()),nDriftBins, driftMin, driftMax, nS2widthBins, s2widthMin, s2widthMax);  htmp->SetTitle("; drift [us]; S2 width [us]");
    htmp = m_hists->BookHist( Form("n%s/h_n%s_s2width_cZ", CXtoStr(cx).Data(), CXtoStr(cx).Data()), nZbins, zMin, zMax, nS2widthBins, s2widthMin, s2widthMax);  htmp->SetTitle("; Z_{c} [cm]; S2 width [us]");
    htmp = m_hists->BookHist( Form("n%s/h_n%s_s2width_cR2",CXtoStr(cx).Data(), CXtoStr(cx).Data()), nR2bins, r2min, r2max, nS2widthBins, s2widthMin, s2widthMax);  htmp->SetTitle("; R_{c}^{2} [cm^{2}]; S2 width [us]");
    
}
}

// Called once per event
void MDC3_WIMPDQ::Execute() {

  m_evt->deriveEvtRQs();

  //cout << "New event. Finished deriving EvtRQs" << endl;

  for (const CX cx : allCXs) {
    if (m_evt->ss->nSingleScatters==1) { // Hacky. Should instead build WIMPDQevent differently.
      if (AccumCX(m_evt,cx,1,{CX::ROI}))           {
	//cout << "Filling first histo" << endl;
	//cout << Form("%s/h_%s_S2_S1",        CXtoStr(cx).Data(), CXtoStr(cx).Data()) << endl;
	m_hists->GetHist(Form("%s/h_%s_S2_S1",        CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill(m_evt->ss->s1Area_phd, m_evt->ss->s2Area_phd);
      }
      //cout << "Finished first histo" << endl;
      if (AccumCX(m_evt,cx,1,{CX::ROI}))           m_hists->GetHist(Form("%s/h_%s_cS2_cS1",      CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedS1Area_phd, m_evt->ss->correctedS2Area_phd);
      if (AccumCX(m_evt,cx,1,{CX::ROI}))           m_hists->GetHist(Form("%s/h_%s_cS2_cS1wimp",      CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedS1Area_phd, m_evt->ss->correctedS2Area_phd);
      if (AccumCX(m_evt,cx,1,{CX::ROI}))           m_hists->GetHist(Form("%s/h_%s_cS2_cS1henr",      CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedS1Area_phd, m_evt->ss->correctedS2Area_phd);
      if (AccumCX(m_evt,cx,1,{CX::ROI}))           m_hists->GetHist(Form("%s/h_%s_cS2log_cS1",   CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedS1Area_phd, TMath::Log10(m_evt->ss->correctedS2Area_phd));
      if (AccumCX(m_evt,cx,1,{CX::ROI}))           m_hists->GetHist(Form("%s/h_%s_cS2cS1_cS1",   CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedS1Area_phd, m_evt->ss->correctedS2Area_phd / m_evt->ss->correctedS1Area_phd);
      if (AccumCX(m_evt,cx,1,{CX::FIDZ,CX::FIDR})) m_hists->GetHist(Form("%s/h_%s_r2_drift",     CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_r2, m_evt->ss->driftTime_ns*-1.e-3);
      if (AccumCX(m_evt,cx,1,{CX::FIDZ,CX::FIDR})) m_hists->GetHist(Form("%s/h_%s_cR2_cZ",       CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, m_evt->ss->correctedZ_cm);
      if (AccumCX(m_evt,cx,1,{CX::FIDR}))          m_hists->GetHist(Form("%s/h_%s_x_y",          CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->x_cm, m_evt->ss->y_cm);
      if (AccumCX(m_evt,cx,1,{CX::FIDR}))          m_hists->GetHist(Form("%s/h_%s_cX_cY",        CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedX_cm, m_evt->ss->correctedY_cm);
      if (AccumCX(m_evt,cx,1,{CX::FIDR, CX::ROI})) m_hists->GetHist(Form("%s/h_%s_cS1_cR2",      CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, m_evt->ss->correctedS1Area_phd);
      if (AccumCX(m_evt,cx,1,{CX::FIDR}))          m_hists->GetHist(Form("%s/h_%s_cS2_cR2",      CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, m_evt->ss->correctedS2Area_phd);
      if (AccumCX(m_evt,cx,1,{CX::FIDR}))          m_hists->GetHist(Form("%s/h_%s_cS2cS1_cR2",   CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, m_evt->ss->correctedS2Area_phd / m_evt->ss->correctedS1Area_phd);
      if (AccumCX(m_evt,cx,1,{CX::FIDZ, CX::ROI})) m_hists->GetHist(Form("%s/h_%s_cS1_cZ",       CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedZ_cm, m_evt->ss->correctedS1Area_phd);
      if (AccumCX(m_evt,cx,1,{CX::FIDZ}))          m_hists->GetHist(Form("%s/h_%s_cS2_cZ",       CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedZ_cm, m_evt->ss->correctedS2Area_phd);
      if (AccumCX(m_evt,cx,1,{CX::FIDZ}))          m_hists->GetHist(Form("%s/h_%s_cS2cS1_cZ",    CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedZ_cm, m_evt->ss->correctedS2Area_phd / m_evt->ss->correctedS1Area_phd);
      if (AccumCX(m_evt,cx,1,{CX::FIDZ}))          m_hists->GetHist(Form("%s/h_%s_s1tba_cZ",     CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedZ_cm, (*m_evt->tpc.tba)[m_evt->ss->s1PulseID]);
      if (AccumCX(m_evt,cx,1,{CX::FIDZ}))          m_hists->GetHist(Form("%s/h_%s_s2tba_cZ",     CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedZ_cm, (*m_evt->tpc.tba)[m_evt->ss->s2PulseID]);
      if (AccumCX(m_evt,cx,1,{CX::FIDZ}))          m_hists->GetHist(Form("%s/h_%s_s1tba_drift",  CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->driftTime_ns*1.e-3, (*m_evt->tpc.tba)[m_evt->ss->s1PulseID]);
      if (AccumCX(m_evt,cx,1,{CX::FIDZ}))          m_hists->GetHist(Form("%s/h_%s_s2tba_drift",  CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->driftTime_ns*1.e-3, (*m_evt->tpc.tba)[m_evt->ss->s2PulseID]);
      if (AccumCX(m_evt,cx,1,{CX::FIDR}))          m_hists->GetHist(Form("%s/h_%s_s1tba_cR2",    CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, (*m_evt->tpc.tba)[m_evt->ss->s1PulseID]);
      if (AccumCX(m_evt,cx,1,{CX::FIDR}))          m_hists->GetHist(Form("%s/h_%s_s2tba_cR2",    CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, (*m_evt->tpc.tba)[m_evt->ss->s2PulseID]);
      if (AccumCX(m_evt,cx,1,{CX::FIDZ}))          m_hists->GetHist(Form("%s/h_%s_s2width_cZ",   CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedZ_cm, ((*m_evt->tpc.aft90)[m_evt->ss->s2PulseID] - (*m_evt->tpc.aft10)[m_evt->ss->s2PulseID])*1.e-3);
      if (AccumCX(m_evt,cx,1,{CX::FIDR}))          m_hists->GetHist(Form("%s/h_%s_s2width_cR2",  CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, ((*m_evt->tpc.aft90)[m_evt->ss->s2PulseID] - (*m_evt->tpc.aft10)[m_evt->ss->s2PulseID])*1.e-3);
      if (AccumCX(m_evt,cx,1,{CX::FIDZ}))          m_hists->GetHist(Form("%s/h_%s_s2width_drift",CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->driftTime_ns*1.e-3, ((*m_evt->tpc.aft90)[m_evt->ss->s2PulseID] - (*m_evt->tpc.aft10)[m_evt->ss->s2PulseID])*1.e-3);
      //cout << "Finished last real histo" << endl;

      // Anti histos
      if (AccumCX(m_evt,cx,0,{CX::ROI}))           m_hists->GetHist(Form("n%s/h_n%s_S2_S1",        CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->s1Area_phd, m_evt->ss->s2Area_phd);
      if (AccumCX(m_evt,cx,0,{CX::ROI}))           m_hists->GetHist(Form("n%s/h_n%s_cS2_cS1wimp",  CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedS1Area_phd, m_evt->ss->correctedS2Area_phd);
      if (AccumCX(m_evt,cx,0,{CX::ROI}))           m_hists->GetHist(Form("n%s/h_n%s_cS2_cS1henr",  CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedS1Area_phd, m_evt->ss->correctedS2Area_phd);
      //cout << "Finished first anti histo" << endl;
      if (AccumCX(m_evt,cx,0,{CX::ROI}))           m_hists->GetHist(Form("n%s/h_n%s_cS2_cS1",      CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedS1Area_phd, m_evt->ss->correctedS2Area_phd);
      if (AccumCX(m_evt,cx,0,{CX::ROI}))           m_hists->GetHist(Form("n%s/h_n%s_cS2log_cS1",   CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedS1Area_phd, TMath::Log10(m_evt->ss->correctedS2Area_phd));
      if (AccumCX(m_evt,cx,0,{CX::ROI}))           m_hists->GetHist(Form("n%s/h_n%s_cS2cS1_cS1",   CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedS1Area_phd, m_evt->ss->correctedS2Area_phd / m_evt->ss->correctedS1Area_phd);
      if (AccumCX(m_evt,cx,0,{CX::FIDZ,CX::FIDR})) m_hists->GetHist(Form("n%s/h_n%s_r2_drift",     CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_r2, m_evt->ss->driftTime_ns*-1.e-3);
      if (AccumCX(m_evt,cx,0,{CX::FIDZ,CX::FIDR})) m_hists->GetHist(Form("n%s/h_n%s_cR2_cZ",       CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, m_evt->ss->correctedZ_cm);
      if (AccumCX(m_evt,cx,0,{CX::FIDR}))          m_hists->GetHist(Form("n%s/h_n%s_x_y",          CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->x_cm, m_evt->ss->y_cm);
      if (AccumCX(m_evt,cx,0,{CX::FIDR}))          m_hists->GetHist(Form("n%s/h_n%s_cX_cY",        CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedX_cm, m_evt->ss->correctedY_cm);
      if (AccumCX(m_evt,cx,0,{CX::FIDR, CX::ROI})) m_hists->GetHist(Form("n%s/h_n%s_cS1_cR2",      CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, m_evt->ss->correctedS1Area_phd);
      if (AccumCX(m_evt,cx,0,{CX::FIDR}))          m_hists->GetHist(Form("n%s/h_n%s_cS2_cR2",      CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, m_evt->ss->correctedS2Area_phd);
      if (AccumCX(m_evt,cx,0,{CX::FIDR}))          m_hists->GetHist(Form("n%s/h_n%s_cS2cS1_cR2",   CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, m_evt->ss->correctedS2Area_phd / m_evt->ss->correctedS1Area_phd);
      if (AccumCX(m_evt,cx,0,{CX::FIDZ, CX::ROI})) m_hists->GetHist(Form("n%s/h_n%s_cS1_cZ",       CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedZ_cm, m_evt->ss->correctedS1Area_phd);
      if (AccumCX(m_evt,cx,0,{CX::FIDZ}))          m_hists->GetHist(Form("n%s/h_n%s_cS2_cZ",       CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedZ_cm, m_evt->ss->correctedS2Area_phd);
      if (AccumCX(m_evt,cx,0,{CX::FIDZ}))          m_hists->GetHist(Form("n%s/h_n%s_cS2cS1_cZ",    CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedZ_cm, m_evt->ss->correctedS2Area_phd / m_evt->ss->correctedS1Area_phd);
      if (AccumCX(m_evt,cx,0,{CX::FIDZ}))          m_hists->GetHist(Form("n%s/h_n%s_s1tba_cZ",     CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedZ_cm, (*m_evt->tpc.tba)[m_evt->ss->s1PulseID]);
      if (AccumCX(m_evt,cx,0,{CX::FIDZ}))          m_hists->GetHist(Form("n%s/h_n%s_s2tba_cZ",     CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedZ_cm, (*m_evt->tpc.tba)[m_evt->ss->s2PulseID]);
      if (AccumCX(m_evt,cx,0,{CX::FIDZ}))          m_hists->GetHist(Form("n%s/h_n%s_s1tba_drift",  CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->driftTime_ns*1.e-3, (*m_evt->tpc.tba)[m_evt->ss->s1PulseID]);
      if (AccumCX(m_evt,cx,0,{CX::FIDZ}))          m_hists->GetHist(Form("n%s/h_n%s_s2tba_drift",  CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->driftTime_ns*1.e-3, (*m_evt->tpc.tba)[m_evt->ss->s2PulseID]);
      if (AccumCX(m_evt,cx,0,{CX::FIDR}))          m_hists->GetHist(Form("n%s/h_n%s_s1tba_cR2",    CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, (*m_evt->tpc.tba)[m_evt->ss->s1PulseID]);
      if (AccumCX(m_evt,cx,0,{CX::FIDR}))          m_hists->GetHist(Form("n%s/h_n%s_s2tba_cR2",    CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, (*m_evt->tpc.tba)[m_evt->ss->s2PulseID]);
      if (AccumCX(m_evt,cx,0,{CX::FIDZ}))          m_hists->GetHist(Form("n%s/h_n%s_s2width_cZ",   CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->correctedZ_cm, ((*m_evt->tpc.aft90)[m_evt->ss->s2PulseID] - (*m_evt->tpc.aft10)[m_evt->ss->s2PulseID])*1.e-3);
      if (AccumCX(m_evt,cx,0,{CX::FIDR}))          m_hists->GetHist(Form("n%s/h_n%s_s2width_cR2",  CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss_R2, ((*m_evt->tpc.aft90)[m_evt->ss->s2PulseID] - (*m_evt->tpc.aft10)[m_evt->ss->s2PulseID])*1.e-3);
      if (AccumCX(m_evt,cx,0,{CX::FIDZ}))          m_hists->GetHist(Form("n%s/h_n%s_s2width_drift",CXtoStr(cx).Data(), CXtoStr(cx).Data()))->Fill( m_evt->ss->driftTime_ns*1.e-3, ((*m_evt->tpc.aft90)[m_evt->ss->s2PulseID] - (*m_evt->tpc.aft10)[m_evt->ss->s2PulseID])*1.e-3);
      }
  }
 
}

// Called after event loop
void MDC3_WIMPDQ::Finalize() {


}

