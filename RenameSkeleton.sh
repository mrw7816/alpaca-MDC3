#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "You must enter a command line arguments for the Analysis name!"
else
    NAME=$1
    echo "Renaming skeleton analysis to -> $NAME"
    
    sed -i "s/MDC3_WIMPDQ/$NAME/g" *.*
    sed -i "s/MDC3_WIMPDQ/$NAME/g" */*.*

    rename MDC3_WIMPDQ $NAME *.c*
    rename MDC3_WIMPDQ $NAME */*.*
fi




